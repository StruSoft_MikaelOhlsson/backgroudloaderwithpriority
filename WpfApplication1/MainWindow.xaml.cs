﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            SmallDataList = new ObservableCollection<SmallData>();
            LoadSmallData(); //Laddning från service
            InitializeComponent();
            listView.ItemsSource = SmallDataList;
            listView.SelectedIndex = 7;

        }
        private ObservableCollection<SmallData> SmallDataList
        {
            get;
            set;
        }
      

        private void LoadSmallData()
        {
            this.SmallDataList.Add(new SmallData("A"));        
            this.SmallDataList.Add(new SmallData("B"));        
            this.SmallDataList.Add(new SmallData("C"));        
            this.SmallDataList.Add(new SmallData("D"));        
            this.SmallDataList.Add(new SmallData("E"));        
            this.SmallDataList.Add(new SmallData("F"));        
            this.SmallDataList.Add(new SmallData("G"));        
            this.SmallDataList.Add(new SmallData("H"));        
            this.SmallDataList.Add(new SmallData("I"));        
            this.SmallDataList.Add(new SmallData("J"));        
            this.SmallDataList.Add(new SmallData("K"));        
            this.SmallDataList.Add(new SmallData("L"));        
        }

        private void MainWindow_OnLoaded( object sender, RoutedEventArgs e )
        {

            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.WorkerReportsProgress = true;
            bgworker.DoWork+=new DoWorkEventHandler(LoadData);
            bgworker.RunWorkerAsync();
        }


        private void LoadData(object sender, DoWorkEventArgs e)
        {
            foreach (var smallData in SmallDataList)
            {

                SmallData selected=null;
                var x=listView.Dispatcher.BeginInvoke((Action)(() => { selected = (SmallData)listView.SelectedItem; }));
                x.Wait();
                if (selected != null && selected.Data == null)
                {
                  
                    selected.Data = new Data("Loaded in advance"); //Laddning från service
                    //Verkar inte som eventet att den har uppdaterats kommer till guit... men det är ju lösbart
                }
                if (smallData.Data != null)
                    continue;
                smallData.Data = new Data("Loaded normally"); //Laddning från service
                //Verkar inte som eventet att den har uppdaterats kommer till guit... men det är ju lösbart
                System.Threading.Thread.Sleep(1000); //Delay loading
            }

        }
    }

    public class SmallData :INotifyPropertyChanged
    {
        private string text;

        public SmallData( string text )
        {
            this.Text = text;
        }
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
                OnPropertyChanged();
            }
        }

        public Data Data
        {
            get;
            set;
        }
        public override string ToString()
        {
            return Text;            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
        {
            var handler = PropertyChanged;
            if( handler != null )
            {
                handler( this, new PropertyChangedEventArgs( propertyName ) );
            }
        }
    }

    public class Data
    {
        public Data( string text )
        {
            this.Text = text;
        }
        public string Text
        {
            get;
            set;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
